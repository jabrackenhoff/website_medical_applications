<h1> Geophysical applications in the medical field </h1>

------

<div style="text-align: justify">
This page contains an overview of the work that the [Seismology and Wave Physics group](https://cos.ethz.ch/) of ETH Zürich performs in their efforts to apply geophysical types of applications to medical problems.

<h2> Motivation </h2>
There are many similarities between seismology in geophysics and ultrasound computed tomography (USCT) in medical imaging.  While both of these imaging applications operate on vastly different spatial scales, they rely on the same underlying physics to model the interaction of mechanical (sound) waves with complex heterogeneous media.
Both seismology and ultrasound tomography utilize the framework provided by inverse theory in order to perform indirect measurements of the quantities of interest, such as the estimation of subsurface material or human tissue properties. Inversion algorithms including efficient strategies to compute gradients such as the adjoint method and uncertainty quantification can thus be adapted to both fields.

This project aims to transfer knowledge between wave-based seismic and medical imaging.
Below is a selection of our work, which shows how the quality of USCT reconstructions can be improved by adapting well-established inversion algorithms from the field of seismology.
</div>